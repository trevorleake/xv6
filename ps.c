#ifdef CS333_P2
#include "types.h"
#include "user.h"

#include "param.h"
#include "uproc.h"


int
main(void)
{
  // malloc an array of uproc objects
  // pass the array into a function called getprocs(size, array);
  // and print the data we care about from teh array.

  int count, i;
  uint max;
  struct uproc *table;

  printf(1, "Name\tPID\tUID\tGID\tPPID\tElapsed\tCPU\tState\t\tSize\n");

  max = 64;
  table = malloc(max * sizeof(struct uproc));
  count = getprocs(max, table);

  for(i=0; i<count; ++i) {
    printf(1, "%s\t", table[i].name);
    printf(1, "%d\t", table[i].pid);
    printf(1, "%d\t", table[i].uid);
    printf(1, "%d\t", table[i].gid);
    printf(1, "%d\t", table[i].ppid);
    printf(1, "%d.%d\t", table[i].elapsed_ticks/1000, table[i].elapsed_ticks%1000);
    printf(1, "%d.%d\t\t", table[i].CPU_total_ticks/1000, table[i].CPU_total_ticks%1000);
    printf(1, "%s\t", table[i].state);
    printf(1, "%d\t\n", table[i].size);
  }
  free(table);

  exit();
}
#endif
