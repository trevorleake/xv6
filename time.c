#ifdef CS333_P2
#include "types.h"
#include "user.h"

#define STRMAX 32

int
main(int argc, char* argv[])
{
  int i, j;
  char** args;
  int parent_pid, ret, ticks;

  args = (char **) malloc((argc-1) * sizeof(char*));
  for(i=0; i<argc-1; ++i) {
    args[i] = (char *) malloc(STRMAX * sizeof(char));
    for(j=0; j<STRMAX; ++j)
      args[i][j] = '\0';
    strcpy(args[i], argv[i+1]);
  }

  // Grab the pid of the time process
  parent_pid = getpid();
  ret = fork();

  ticks = uptime();

  // If this is not the original time process
  if(parent_pid != getpid())
    exec(argv[1], args);

  wait();
  ticks = uptime() - ticks;

  if(parent_pid == getpid())
    printf(1, "%s ran for %d.%d seconds.\n", argv[1], ticks/1000, ticks%1000);

  if(ret)
    ret = 0;

  exit();
}
#endif
